package net.davidev.ejemplotabs;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TabHost;

public class MainActivity extends AppCompatActivity {

    TabHost tabHost;
    TabHost.TabSpec tb1, tb2, tb3, tb4;

    Button btn_loros;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btn_loros = findViewById(R.id.btn_loros);

        tabHost = findViewById(R.id.tabHost);
        tabHost.setup(); // Si da error aquí, revisar los id del TabWidget y el FrameLayout


        tb1 = tabHost.newTabSpec("Mitab1");
        tb1.setContent(R.id.tab_gatos); // Enlaza con el id del include de la tab
        // Titulo de la pestaña, el segundo parametro es para un icono (es opcional)
        tb1.setIndicator("Gatos");

        tb2 = tabHost.newTabSpec("Mitab2");
        tb2.setContent(R.id.tab_perros);
        tb2.setIndicator("Perros");

        tb3 = tabHost.newTabSpec("Mitab3");
        tb3.setContent(R.id.tab_loros);
        tb3.setIndicator("Loros");

        tb4 = tabHost.newTabSpec("Mitab4");
        tb4.setContent(R.id.tab_cam);
        // Ejemplo de tab solo con icono, se envía el label vacío
        tb4.setIndicator("",getDrawable(R.drawable.ic_cam));

        // El orden de las tabs se decide aquí (empieza en 0)
        tabHost.addTab(tb4);
        tabHost.addTab(tb1);
        tabHost.addTab(tb2);
        tabHost.addTab(tb3);

        // Cambia la tab activa
        tabHost.setCurrentTab(1); // tab gatos

        // La logica de todas las tabs se puede manejar desde este .java

        btn_loros.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Cambia la tab activa
                tabHost.setCurrentTab(2); // tab loros
            }
        });

    }
}
